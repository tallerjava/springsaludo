/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connectis.saludo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Franco
 */
@Controller
public class SaludoController {

    @GetMapping(value = "/formulario")
    public String formulario() {
        return "formulario";
    }

    @RequestMapping(value = "/saludar")
    public String saludar(@RequestParam String nombre, Model modelo) {
        modelo.addAttribute("nombre", nombre);
        return "respuestasaludo";
    }

}
